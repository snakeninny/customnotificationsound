#ifndef kCFCoreFoundationVersionNumber_iOS_8_0
#define kCFCoreFoundationVersionNumber_iOS_8_0 1140.10
#endif

#ifndef kCFCoreFoundationVersionNumber_iOS_9_0
#define kCFCoreFoundationVersionNumber_iOS_9_0 1240.10
#endif

@interface PSSpecifier : NSObject
@property (retain, nonatomic) NSString *identifier;
- (id)propertyForKey:(NSString *)key;
@end

@interface PSViewController : UIViewController
- (PSSpecifier *)specifier;
@end

@interface PSListController : PSViewController
- (UITableView *)table;
@end

@interface BBContent : NSObject
@property (copy, nonatomic) NSString *message;
@property (copy, nonatomic) NSString *subtitle;
@property (copy, nonatomic) NSString *title;
@end

@interface BBBulletin : NSObject
@property (copy, nonatomic) NSString *section;
@property (retain, nonatomic) BBContent *content;
@end

@interface BBSound : NSObject
+ (instancetype)alertSoundWithSystemSoundPath:(id)arg1; // iOS 8
+ (instancetype)alertSoundWithSystemSoundID:(unsigned)arg1; // iOS 7
@end

@interface PSTableCell : UITableViewCell
@property (retain, nonatomic) PSSpecifier *specifier;
@end

@interface BBSectionInfo : NSObject
@property (copy, nonatomic) NSString *sectionID;
@property (copy, nonatomic) NSString *displayName;
@end

@interface BulletinBoardAppDetailController : PSListController 
- (NSNumber *)CNSSoundsValue;
- (BOOL)CNSIsSoundsSectionInCell:(PSTableCell *)visibleCell;
- (void)CNSCustomizeSoundCell:(PSTableCell *)cell;
// - (BBSectionInfo *)effectiveSectionInfo; // iOS 8
// - (BBSectionInfo *)_effectiveSectionInfoForSectionInfo:(id)arg1 pushSetting:(NSUInteger)arg2; // iOS 9
- (NSNumber *)_valueOfNotificationType:(NSUInteger)arg1; // iOS 8
- (NSNumber *)_valueOfNotificationType:(NSUInteger)arg1 forSectionInfo:(BBSectionInfo *)arg2; // iOS 9
- (void)_setValue:(NSNumber *)arg1 notificationType:(NSUInteger)arg2; // iOS 8
- (void)_setValue:(NSNumber *)arg1 notificationType:(NSUInteger)arg2 forSectionInfo:(BBSectionInfo *)arg3; // iOS 9
@end
